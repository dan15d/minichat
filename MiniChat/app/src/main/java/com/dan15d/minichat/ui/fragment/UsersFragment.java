package com.dan15d.minichat.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dan15d.minichat.App;
import com.dan15d.minichat.R;
import com.dan15d.minichat.injector.module.UsersModule;
import com.dan15d.minichat.model.User;
import com.dan15d.minichat.ui.adapter.UsersAdapter;
import com.dan15d.minichat.ui.presenter.UsersPresenter;
import com.dan15d.minichat.ui.views.Main;
import com.dan15d.minichat.ui.views.Users;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UsersFragment extends Fragment implements Users{

    @BindView(R.id.recycler) RecyclerView recycler;

    @Inject UsersPresenter presenter;
    Main main;

    public UsersFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_users, container, false);
        ButterKnife.bind(this,view);
        inject();
        main = (Main) getActivity();

        presenter.getUsers();

        return view;
    }

    private void inject(){
        App app = (App) getActivity().getApplication();
        app.getAppComponent().plus(new UsersModule(this)).inject(this);
    }


    @Override
    public void onCompleteUsers(List<User> users) {
        initRecyclerView(users);
    }

    public void initRecyclerView(List<User> users) {
        UsersAdapter adapter = new UsersAdapter((ArrayList<User>) users, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(mLayoutManager);
        recycler.setItemAnimator(new DefaultItemAnimator());

        recycler.setAdapter(adapter);


        main.onUserToChatChange(users.get(0).getId());
    }

    @Override
    public void onErrorUsers() {

    }

    @Override
    public void onClickUser(int idUser) {
        main.onUserToChatChange(idUser);
    }
}
