package com.dan15d.minichat.model;

/**
 * Created by dfeito on 20/06/2017.
 */

public class Message {
    private int idUser;
    private String message;

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
