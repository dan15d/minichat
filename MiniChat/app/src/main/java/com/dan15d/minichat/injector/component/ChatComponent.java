package com.dan15d.minichat.injector.component;

import com.dan15d.minichat.injector.module.ChatModule;
import com.dan15d.minichat.injector.module.UsersModule;
import com.dan15d.minichat.ui.fragment.ChatFragment;
import com.dan15d.minichat.ui.fragment.UsersFragment;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by dfeito on 14/03/2017.
 */

@Singleton
@Subcomponent(modules = {ChatModule.class})
public interface ChatComponent {

     void inject(ChatFragment fragment);

}

