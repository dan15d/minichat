package com.dan15d.minichat.injector.module;


import com.dan15d.minichat.datasource.ChatDataSource;
import com.dan15d.minichat.datasource.UserDataSource;
import com.dan15d.minichat.ui.fragment.ChatFragment;
import com.dan15d.minichat.ui.fragment.UsersFragment;
import com.dan15d.minichat.ui.presenter.ChatPresenter;
import com.dan15d.minichat.ui.presenter.UsersPresenter;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;

/**
 * Created by dfeito on 14/03/2017.
 */

@Module
public class ChatModule {

    public ChatFragment fragment;

    public ChatModule(ChatFragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    public ChatPresenter provideMapPresenter(@Named("ui_thread") Scheduler uiThread,
                                             @Named("executor_thread") Scheduler executorThread,
                                             FirebaseDatabase database) {

        return new ChatPresenter(fragment,provideChatDataSource(database),uiThread,executorThread);
    }



    @Provides
    public ChatDataSource provideChatDataSource (FirebaseDatabase database)  {
        return new ChatDataSource(database);
    }
}
