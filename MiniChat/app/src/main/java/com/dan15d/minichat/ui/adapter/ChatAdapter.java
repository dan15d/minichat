package com.dan15d.minichat.ui.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dan15d.minichat.App;
import com.dan15d.minichat.R;
import com.dan15d.minichat.model.Message;
import com.dan15d.minichat.model.User;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dfeito on 26/05/2016.
 */
public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {


    private ArrayList<Message> messages;


    public ChatAdapter(ArrayList<Message> messages){
        this.messages = messages;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_chat, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {

        final Message message = messages.get(i);

        viewHolder.tv_text.setText(message.getMessage());

        if (App.actualUserId != message.getIdUser()){
            viewHolder.ll_content.setGravity(Gravity.LEFT);
            viewHolder.ll_content_message.setBackgroundColor(Color.WHITE);
        }
        else{
            viewHolder.ll_content.setGravity(Gravity.RIGHT);
            viewHolder.ll_content_message.setBackgroundColor(0xFFFB8C00);
        }

    }



    @Override
    public int getItemCount() {
        return messages != null ? messages.size() : 0;
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_text) TextView tv_text;
        @BindView(R.id.ll_content_message) LinearLayout ll_content_message;
        @BindView(R.id.ll_content) LinearLayout ll_content;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }


}
