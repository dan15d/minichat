package com.dan15d.minichat.datasource;

import android.util.Log;

import com.dan15d.minichat.model.Message;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.kelvinapps.rxfirebase.DataSnapshotMapper;
import com.kelvinapps.rxfirebase.RxFirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by dfeito on 20/06/2017.
 */

public class ChatDataSource {

    FirebaseDatabase database;

    public ChatDataSource (FirebaseDatabase database){
        this.database = database;
    }


    public Observable<List<Message>> getChatReference(int idUser, int idUserChat){
        String idChat = String.valueOf(idUser+idUserChat);

        DatabaseReference myRef = database.getReference()
                .child("chats").child(idChat);

        return RxFirebaseDatabase
                .observeValueEvent(myRef, DataSnapshotMapper.listOf(Message.class));

    }

    public void sendMessage(int idUser, int idUserChat,Message message){
        String idChat = String.valueOf(idUser+idUserChat);

        DatabaseReference myRef = database.getReference()
                .child("chats").child(idChat);

        RxFirebaseDatabase.observeSingleValueEvent(myRef, DataSnapshotMapper.listOf(Message.class))
                .subscribe(posts->{
                            String postSize = String.valueOf(posts.size());
                            myRef.child(postSize).setValue(message);
                        },
                        error->{
                            error.getMessage();
                        });

    }
}
