package com.dan15d.minichat.ui.views;

import android.content.SharedPreferences;


/**
 * Created by dfeito on 14/03/2017.
 */

public interface Base {

    void showProgressBar();
    void hideProgressBar();
    void internetError();

}
