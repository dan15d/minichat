package com.dan15d.minichat.datasource;

import android.util.Log;

import com.dan15d.minichat.model.Message;
import com.dan15d.minichat.model.User;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.kelvinapps.rxfirebase.DataSnapshotMapper;
import com.kelvinapps.rxfirebase.RxFirebaseDatabase;

import java.util.List;

import rx.Observable;

/**
 * Created by dfeito on 20/06/2017.
 */

public class UserDataSource {

    FirebaseDatabase database;

    public UserDataSource (FirebaseDatabase database){
        this.database = database;
    }


    public Observable<List<User>> getUsers(){
        DatabaseReference myRef = database.getReference()
                .child("users");

        return RxFirebaseDatabase
                .observeSingleValueEvent(myRef, DataSnapshotMapper.listOf(User.class));

    }
}
