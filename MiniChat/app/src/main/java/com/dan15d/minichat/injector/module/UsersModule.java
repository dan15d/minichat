package com.dan15d.minichat.injector.module;


import com.dan15d.minichat.datasource.UserDataSource;
import com.dan15d.minichat.ui.fragment.UsersFragment;
import com.dan15d.minichat.ui.presenter.UsersPresenter;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import rx.Scheduler;

/**
 * Created by dfeito on 14/03/2017.
 */

@Module
public class UsersModule {

    public UsersFragment fragment;

    public UsersModule(UsersFragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    public UsersPresenter provideUsersPresenter(@Named("ui_thread") Scheduler uiThread,
                                              @Named("executor_thread") Scheduler executorThread,
                                              FirebaseDatabase database) {

        return new UsersPresenter(fragment,provideUserDataSource(database),uiThread,executorThread);
    }



    @Provides
    public UserDataSource provideUserDataSource (FirebaseDatabase database)  {
        return new UserDataSource(database);
    }
}
