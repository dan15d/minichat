package com.dan15d.minichat.ui.adapter;

import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dan15d.minichat.App;
import com.dan15d.minichat.R;
import com.dan15d.minichat.model.User;
import com.dan15d.minichat.ui.views.Chat;
import com.dan15d.minichat.ui.views.Users;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dfeito on 26/05/2016.
 */
public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {


    private ArrayList<User> users;
    private Users callback;
    private ArrayList<View> backgrounds = new ArrayList<>();


    public UsersAdapter(ArrayList<User> users, Users callback){
        this.users = users;
        this.callback = callback;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_user, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {

        final User user = users.get(i);

        backgrounds.add(viewHolder.itemView);
        viewHolder.tv_name.setText(user.getName());

        if (i==0) viewHolder.itemView.setBackgroundColor(0xFFF57C00);

        viewHolder.itemView.setOnClickListener(l -> {
            callback.onClickUser(user.getId());
            setDefaultAllBackgrounds();
            viewHolder.itemView.setBackgroundColor(0xFFF57C00);
        });
    }


    private void setDefaultAllBackgrounds(){
        for (View view : backgrounds){
            view.setBackgroundColor(0xFFFFB74D);
        }
    }

    @Override
    public int getItemCount() {
        return users != null ? users.size() : 0;
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_name) TextView tv_name;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }


}
