package com.dan15d.minichat.model;

import java.util.ArrayList;

/**
 * Created by dfeito on 20/06/2017.
 */

public class Chat {
    ArrayList<Message> messages;

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }
}
