package com.dan15d.minichat.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.dan15d.minichat.App;
import com.dan15d.minichat.R;
import com.dan15d.minichat.injector.module.ChatModule;
import com.dan15d.minichat.model.Message;
import com.dan15d.minichat.model.User;
import com.dan15d.minichat.ui.adapter.ChatAdapter;
import com.dan15d.minichat.ui.adapter.UsersAdapter;
import com.dan15d.minichat.ui.presenter.ChatPresenter;
import com.dan15d.minichat.ui.views.Chat;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChatFragment extends Fragment implements Chat {

    @BindView(R.id.recycler) RecyclerView recycler;
    @BindView(R.id.et_message) EditText et_message;

    @Inject ChatPresenter presenter;
    int idUserChat;

    public ChatFragment() {}

    public static ChatFragment newInstance(int idUserChat) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();

        args.putInt("idUserChat",idUserChat);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        ButterKnife.bind(this,view);
        inject();

        idUserChat = getArguments().getInt("idUserChat");
        presenter.getChat(idUserChat);

        return view;
    }

    private void inject(){
        App app = (App) getActivity().getApplication();
        app.getAppComponent().plus(new ChatModule(this)).inject(this);
    }

    @Override
    public void onChatChange(ArrayList<Message> messages) {
        initRecyclerView(messages);
    }

    public void initRecyclerView(List<Message> messages) {
        ChatAdapter adapter = new ChatAdapter((ArrayList<Message>) messages);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(mLayoutManager);
        recycler.setItemAnimator(new DefaultItemAnimator());

        recycler.setAdapter(adapter);

        recycler.scrollToPosition(messages.size()-1);

    }

    @Override
    public void onChatError() {

    }


    @OnClick(R.id.iv_send)
    public void onClickSend(){
        String message = et_message.getText().toString();


        if (!message.equals("")) {
            presenter.sendChat(idUserChat, message);
            et_message.setText("");
        }

    }
}
