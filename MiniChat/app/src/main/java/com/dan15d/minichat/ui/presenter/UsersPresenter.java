package com.dan15d.minichat.ui.presenter;

import com.dan15d.minichat.App;
import com.dan15d.minichat.datasource.UserDataSource;
import com.dan15d.minichat.model.User;
import com.dan15d.minichat.ui.views.Base;
import com.dan15d.minichat.ui.views.Users;

import javax.xml.transform.sax.TemplatesHandler;

import rx.Scheduler;

/**
 * Created by dfeito on 20/06/2017.
 */

public class UsersPresenter {

    private UserDataSource dataSource;
    private Scheduler uiThread;
    private Scheduler executorThread;

    Users callback;

    public UsersPresenter(Users callback,
                          UserDataSource dataSource,
                          Scheduler uiThread,
                          Scheduler executorThread){

        this.callback = callback;
        this.dataSource = dataSource;
        this.uiThread = uiThread;
        this.executorThread = executorThread;
    }

    public void getUsers(){
        dataSource.getUsers()
                .observeOn(uiThread)
                .subscribeOn(executorThread)
                .subscribe(
                        users->{
                            int indexToDelete = 0;

                            //delete current app user
                            for (int i = 0; i < users.size(); i++) {
                                if (users.get(i).getId() == App.actualUserId)
                                    indexToDelete = i;
                            }

                            users.remove(indexToDelete);
                            callback.onCompleteUsers(users);
                        },
                        error->{
                            callback.onErrorUsers();
                        }
                );
    }
}
