package com.dan15d.minichat.injector.component;



import com.dan15d.minichat.App;
import com.dan15d.minichat.injector.module.AppModule;
import com.dan15d.minichat.injector.module.ChatModule;
import com.dan15d.minichat.injector.module.UsersModule;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import rx.Scheduler;

/**
 * Created by dfeito on 27/03/2017.
 */

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    void inject(App app);

    UsersComponent plus(UsersModule module);
    ChatComponent plus(ChatModule module);

    App provideApplicationContext();

    @Named("ui_thread") Scheduler uiThread();
    @Named("executor_thread") Scheduler executorThread();
}
