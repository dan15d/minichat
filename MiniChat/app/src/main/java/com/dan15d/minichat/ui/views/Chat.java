package com.dan15d.minichat.ui.views;

import com.dan15d.minichat.model.Message;

import java.util.ArrayList;

/**
 * Created by dfeito on 20/06/2017.
 */

public interface Chat {
    void onChatChange(ArrayList<Message> messages);
    void onChatError();
}
