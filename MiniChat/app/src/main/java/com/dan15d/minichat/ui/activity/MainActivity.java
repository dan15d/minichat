package com.dan15d.minichat.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.FrameLayout;

import com.dan15d.minichat.R;
import com.dan15d.minichat.common.BaseActivity;
import com.dan15d.minichat.datasource.ChatDataSource;
import com.dan15d.minichat.ui.fragment.ChatFragment;
import com.dan15d.minichat.ui.fragment.UsersFragment;
import com.dan15d.minichat.ui.views.Base;
import com.dan15d.minichat.ui.views.Main;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements Main {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initUserFragment();

    }

    public void initUserFragment(){
        UsersFragment usersFragment = new UsersFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fl_users, usersFragment)
                .commit();
    }

    public void initChatFragment(int idUserChat){
        ChatFragment chatFragment = ChatFragment.newInstance(idUserChat);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fl_chat, chatFragment)
                .commit();
    }

    @Override
    public void onUserToChatChange(int idUserChat) {
        initChatFragment(idUserChat);
    }
}
