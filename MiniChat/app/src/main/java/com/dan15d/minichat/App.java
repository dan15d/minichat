package com.dan15d.minichat;

import android.app.Application;

import com.dan15d.minichat.injector.component.AppComponent;
import com.dan15d.minichat.injector.component.DaggerAppComponent;
import com.dan15d.minichat.injector.module.AppModule;

/**
 * Created by dfeito on 27/03/2017.
 */

public class App extends Application {

    public static int actualUserId = 1;

    AppComponent appComponent;

    @Override
    public void onCreate(){
        super.onCreate();
        appComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .build();
        appComponent.inject(this);


    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
