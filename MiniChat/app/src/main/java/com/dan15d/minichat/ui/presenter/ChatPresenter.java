package com.dan15d.minichat.ui.presenter;

import com.dan15d.minichat.App;
import com.dan15d.minichat.datasource.ChatDataSource;
import com.dan15d.minichat.datasource.UserDataSource;
import com.dan15d.minichat.model.Message;
import com.dan15d.minichat.ui.views.Chat;
import com.dan15d.minichat.ui.views.Users;

import java.util.ArrayList;

import rx.Scheduler;

/**
 * Created by dfeito on 20/06/2017.
 */

public class ChatPresenter {

    private ChatDataSource dataSource;
    private Scheduler uiThread;
    private Scheduler executorThread;

    Chat callback;

    public ChatPresenter(Chat callback,
                         ChatDataSource dataSource,
                         Scheduler uiThread,
                         Scheduler executorThread){

        this.callback = callback;
        this.dataSource = dataSource;
        this.uiThread = uiThread;
        this.executorThread = executorThread;
    }

    public void getChat(int idUserChat){
        dataSource.getChatReference(App.actualUserId,idUserChat)
                .observeOn(uiThread)
                .subscribeOn(executorThread)
                .subscribe(
                        posts->{
                            callback.onChatChange((ArrayList<Message>) posts);
                        },
                        error->{
                            callback.onChatError();
                        }
                );
    }

    public void sendChat(int idUserChat, String text){
        Message message = new Message();
        message.setIdUser(App.actualUserId);
        message.setMessage(text);

        dataSource.sendMessage(App.actualUserId, idUserChat,message);
    }
}
