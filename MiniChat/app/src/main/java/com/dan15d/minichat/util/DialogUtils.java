package com.dan15d.minichat.util;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;


/**
 * Created by dfeito on 30/05/2016.
 */


public class DialogUtils {

    public static Dialog dialog;


    public static AlertDialog.Builder createSimpleDialog(Context context, String title, String menssagge){

        return new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(menssagge)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss());
    }

}
