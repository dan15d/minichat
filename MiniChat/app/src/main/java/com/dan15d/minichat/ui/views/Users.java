package com.dan15d.minichat.ui.views;

import com.dan15d.minichat.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dfeito on 20/06/2017.
 */

public interface Users {
    void onCompleteUsers(List<User> users);
    void onErrorUsers();

    void onClickUser(int idUser);
}
