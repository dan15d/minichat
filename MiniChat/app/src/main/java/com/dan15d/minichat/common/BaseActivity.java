package com.dan15d.minichat.common;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.dan15d.minichat.ui.views.Base;
import com.dan15d.minichat.util.DialogUtils;



/**
 * Created by dfeito on 14/03/2017.
 */

public class BaseActivity extends AppCompatActivity implements Base {

    public ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);

    }

    @Override
    public void showProgressBar() {
        progressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void internetError() {
        DialogUtils.createSimpleDialog(this,"Error","No dispone de conexión a internet").show();
    }








}
